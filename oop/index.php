<?php 

require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

$sheep = new Animal("shaun");

echo "Name :" .$sheep->name . "<br>";
echo "Legs :" .$sheep->legs . "<br>";
echo "Cold Blooded :" .$sheep->cold_blooded . "<br><br>"; 

$kodok = new Frog("buduk");

echo "Name : " .$kodok->name . "<br>";
echo "Legs : " .$kodok->legs . "<br>";
echo "Cold Blooded : " .$kodok->cold_blooded . "<br>";
echo $kodok->jump() . "<br><br>";

$sungkong = new Ape("kera sakti");

echo "Name :" .$sungkong->name . "<br>";
echo "Legs :" .$sungkong->legs . "<br>";
echo "Cold Blooded :" .$sungkong->cold_blooded . "<br>";
echo $sungkong->Yell();


?>

